package jav;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.*;
import static org.junit.Assert.*;

import org.junit.experimental.categories.Category;

@Category(IntegrationTest.class)
public class LoginFunctionalTest {

    static WebDriver driver;

    @BeforeClass
    public static void setup() {
        //driver = new ChromeDriver();
        
              //  FirefoxBinary firefoxBinary = new FirefoxBinary();
        //firefoxBinary.addCommandLineOptions("--headless"); 
    	DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
       System.setProperty("webdriver.gecko.driver", "C:\\geckodriver-v0.23.0-win64\\geckodriver.exe");
      
       driver = new FirefoxDriver();
       // FirefoxOptions firefoxOptions = new FirefoxOptions();
      //  firefoxOptions.setBinary(firefoxBinary);
        
       // driver = new FirefoxDriver(firefoxOptions);
    }

    /*@AfterClass
    public static void cleanUp() {
        driver.quit();
    }*/

    @Test
    public void loginSuccess() throws InterruptedException {
        driver.get("http://localhost:5050/DevApp");
        WebElement first_name = driver.findElement(By.name("first_name"));
        WebElement last_name = driver.findElement(By.name("last_name"));
        WebElement username = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement address = driver.findElement(By.name("address"));
        WebElement contact = driver.findElement(By.name("contact"));
        WebElement button = driver.findElement(By.xpath("/html/body/form/input"));         
        first_name.sendKeys("Yashaswini");
        last_name.sendKeys("Kaza");
        username.sendKeys("Kaza");
        password.sendKeys("Kaza");
        address.sendKeys("Kaza");
        contact.sendKeys("Kaza");
        button.click();
        assertTrue(driver.getPageSource().contains("Display"));
    }
    
    @Test
    public void loginFail() {
        driver.get("http://localhost:5050/DevApp");
        WebElement first_name = driver.findElement(By.name("first_name"));
        WebElement last_name = driver.findElement(By.name("last_name"));
        WebElement username = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement address = driver.findElement(By.name("address"));
        WebElement contact = driver.findElement(By.name("contact"));
        
        WebElement button = driver.findElement(By.xpath("/html/body/form/input"));         
        first_name.sendKeys("Yashaswini");
        last_name.sendKeys("Kaza");
        username.sendKeys("Kaza");
        password.sendKeys("Kaza");
        address.sendKeys("Kaza");
        contact.sendKeys("Kaza");
        button.click();
        assertTrue(driver.getPageSource().contains("Fail!!"));
        }
    
    
    
    
}

